var expect = require('chai').expect;

var { Builder, By, Key, until } = require('selenium-webdriver');
var twilio = require('./twilio.js')

describe("SMS Authentication", () => {

    const URL ="http://68.183.57.185:8003/"
    const phoneNumber = "6479319419"
  
    const driver = new Builder()
  
      .usingServer("http://localhost:4444/wd/hub")
      // Setting capabilities when requesting a new session
      .withCapabilities({"browserName":"chrome"})
      // Creates a new WebDriver client based on this builder's current configuration. 
      .build();
    
      /*
     .forBrowser('chrome')
     .build(); 
     */
    

   it('check Verify endpoint with correct pin', async () => {
        await driver.get(URL);
        await driver.findElement(By.id('phoneNum')).sendKeys(phoneNumber, Key.ENTER);
        await driver.findElement(By.id('send_sms')).click();
        
        // save the date and time when html element by send_sms id has been clicked 
        const date = new Date()
        // wait and read the last SMS after the data
        const sms = await twilio.waitAndReadLastSMS_afterDate(date)
        // SMS content will be 
        // PINCODE is your pin code
        // we will to split this SMS and get the fisrt word 
        var pin = sms.split(" ")
        
        await driver.findElement(By.id('pin')).sendKeys(pin[0], Key.ENTER);
        await driver.findElement(By.id('varified_code')).click();
        
        const msg = "Your Phone Number has been varified"
        await driver.wait(until.elementTextIs(await driver.findElement(By.id('message')), msg), 7 * 10000);  
    });  
    after(async () => driver.quit());

  });
  