### Description
Automated testing a email verification application with Selenium WebDriver and Node.js

---

### Requirements
clone and Run the SMS authentication application before running test.js

```
git clone https://neginmash@bitbucket.org/neginmash/2fa-smsauthentication.git 
```
---

### - test.js 
test.js file contain functional test using [mocha](https://mochajs.org/) test framework and [selenium-webdriver](https://www.npmjs.com/package/selenium-webdriver) browser automation library.

---

### - Bitbuket-pipeline.yml 
Bitbucket Pipelines is an integrated CI/CD service, built into Bitbucket. It allows you to automatically test based on a Bitbuket-pipeline.yml in your repository.  

--- 

### - twilio.js 
##### What is twilio?
Twilio is a developer platform for communications. Software teams use Twilio APIs to add capabilities like voice, video, and messaging to their applications. More info about twilio visit [twilio docs](https://www.twilio.com/docs/)

##### twilio requirements

These are the following parameters we have to retrieve from Twilio account in order to make our test work:

* TWILIO_ACCOUNT_SID
* TWILIO_AUTH_TOKEN
* TWILIO_FROM_NUMBER

##### Twilio API Message resource

A [Message REST API](https://www.twilio.com/docs/sms/api/message-resource) represents an inbound or outbound message. With this REST API, you can:

* Fetch a specific message
* Read a list of messages
