require('dotenv').config()
var accountSid = "ACd551abbc3e95af00134ad4b2d255e91b";
var authToken = "07907ea46041a2d8ec48f6cbeabd4b43";
const client = require('twilio')(accountSid, authToken);

var iteration=1

exports.waitAndReadLastSMS_afterDate = (date)=>{
    iteration=1
    return waitAndReadLastSMS_afterDate(date)
}

waitAndReadLastSMS_afterDate = (date)=>{
    //console.log('iteration ' + iteration++)
    return new Promise((resolve,reject)=>{
       
      // after 2 second of searching reject the promise
        let timeout= setTimeout(()=>{
            reject('timeout')
        },20000)
        
        // search for the last msg that match to these criteria
        client.messages
            .list({
                dateSentAfter: date,
                from: "+16477992529",
                to: "+16479319419",
                limit: 1
            })
            .then(messages => {
                // if it find the msg resolve the promise
                if(messages.length){
                    clearTimeout(timeout)
                    m = messages[0]
                    //console.log('lastmsg datesent', m.dateSent)
                    resolve(m.body)
                      
                }else{
                    // if it did not find the msg call this promise again 
                    setTimeout(()=>{
                        waitAndReadLastSMS_afterDate(date).then((body)=>{
                            resolve(body)
                        }).catch(er=>{
                            reject(er)
                        })
                    },500)
                }
            });
    })
} 


